name := """poloplay"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(SbtWeb)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  filters,
  "org.webjars" %% "webjars-play" % "2.4.0-M2",
  "org.webjars" % "bootswatch-yeti" % "3.3.2",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "d3js" % "3.4.13",
  "org.reflections" % "reflections" % "0.9.9",
  "com.polopoly" % "legacy-site-plugin" % "10.12.0",
  "com.atex.gong" % "source-common" % "1.1"
)

resolvers += "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository"

TwirlKeys.templateImports += "com.polopoly.model._"
