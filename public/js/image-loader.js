$('.img-container').each(function() {
    var container = $(this);
    var targetWidth = container.width() * window.devicePixelRatio;
    var biggestWidth = 0;
    var actualWidth = Number.MAX_VALUE;
    $.each(container.get(0).attributes, function() {
        if (this.specified && this.name.indexOf("data-src-") == 0) {
            var width = parseInt(this.name.substring(9));
            if (width >= targetWidth && width < actualWidth) {
                actualWidth = width;
            }
        }
    });
    var img = $('<img>').attr('src', container.attr('data-src-' + actualWidth)).attr('alt', container.attr('data-alt'));
    container.html(img);
});
