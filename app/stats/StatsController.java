package stats;

import java.util.Map;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import stats.html.treestats;
import stats.html.typestats;

public class StatsController extends Controller {
    public static Result getTypeData() {
        Map<String, Object> stats = StatsCollector.getPerTypeStats();
        if (stats != null) {
            return ok(Json.toJson(stats));
        }
        return noContent();
    }

    public static Result getTreeData() {
        Map<String, Object> stats = StatsCollector.getCallTreeStats();
        if (stats != null) {
            return ok(Json.toJson(stats));
        }
        return noContent();
    }

    public static Result renderTypePage() {
        return ok(typestats.render());
    }

    public static Result renderTreePage() {
        return ok(treestats.render());
    }
}
