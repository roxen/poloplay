package stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import play.twirl.api.Html;
import play.mvc.Controller;
import poloplay.ModelController;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;

public class StatsCollector extends Controller {

    private static final String PER_TYPE_STATS = "stats";
    private static final String CALL_TREE_STATS = "hstats";
    private static final String TOP_CALL_TREE_STATS = "tophstats";
    private static final String TOP_PER_TYPE_STATS = "typeStats";

    private static Map<String, Object> perTypeStats = null;
    private static Map<String, Object> callTreeStats = null;

    public static Map<String, Object> getPerTypeStats() {
        return perTypeStats;
    }

    public static Map<String, Object> getCallTreeStats() {
        return callTreeStats;
    }

    public static Html render(ModelController renderer, Model realModel) throws CMException {
        long now = System.nanoTime();

        Stats parentPerTypeStats = (Stats) ctx().args.get(PER_TYPE_STATS);
        if (parentPerTypeStats != null) {
            parentPerTypeStats.duration += now - parentPerTypeStats.timestamp;
        }
        Stats myPerTypeStats = new Stats(now);
        ctx().args.put(PER_TYPE_STATS, myPerTypeStats);

        Map<String, Object> myCallTreeStats = new HashMap<String, Object>();
        String contentIdStr = null;
        Object modelId = realModel.getId();
        if (modelId instanceof ContentId) {
            contentIdStr = ((ContentId) modelId).getContentId().getContentIdString();
        }
        String modelName = String.valueOf(realModel.getAttribute("name"));
        if (contentIdStr != null) {
            modelName = contentIdStr + ": " + modelName;
        }
        myCallTreeStats.put("name", modelName);
        ArrayList<Object> myHChildren = new ArrayList<Object>();
        myCallTreeStats.put("children", myHChildren);

        @SuppressWarnings("unchecked")
        Map<String, Object> parentCallTreeStats = (Map<String, Object>) ctx().args.get(CALL_TREE_STATS);
        if (parentCallTreeStats != null) {
            @SuppressWarnings("unchecked")
            List<Object> callTreeChildren = (List<Object>) parentCallTreeStats.get("children");
            callTreeChildren.add(myCallTreeStats);
        } else {
            ctx().args.put(TOP_CALL_TREE_STATS, myCallTreeStats);
        }
        ctx().args.put(CALL_TREE_STATS, myCallTreeStats);

        Html html = renderer.render(realModel);

        long after = System.nanoTime();

        myPerTypeStats.duration += after - myPerTypeStats.timestamp;
        logPerTypeStats(realModel, myPerTypeStats.duration);
        if (parentPerTypeStats != null) {
            parentPerTypeStats.timestamp = after;
        }
        ctx().args.put(PER_TYPE_STATS, parentPerTypeStats);

        myCallTreeStats.put("size", myPerTypeStats.duration);
        ctx().args.put(CALL_TREE_STATS, parentCallTreeStats);

        return html;
    }

    @SuppressWarnings("unchecked")
    private static void logPerTypeStats(Model model, long value) {
        String modelTypeName = model.getModelType().getName();
        String contentIdStr = null;
        Object modelId = model.getId();
        if (modelId instanceof ContentId) {
            contentIdStr = ((ContentId) modelId).getContentId().getContentIdString();
        }
        String modelName = String.valueOf(model.getAttribute("name"));
        if (contentIdStr != null) {
            modelName = contentIdStr + ": " + modelName;
        }

        Map<String, Object> stats = getTypeStats();
        Map<String, Object> children = (Map<String, Object>) stats.get("children");
        Map<String, Object> typeMap = (Map<String, Object>) children.get(modelTypeName);
        if (typeMap == null) {
            typeMap = new HashMap<String, Object>();
            typeMap.put("name", modelTypeName);
            typeMap.put("children", new ArrayList<Object>());
            children.put(modelTypeName, typeMap);
        }
        List<Object> elements = (List<Object>) typeMap.get("children");
        HashMap<String, Object> element = new HashMap<String, Object>();
        element.put("name", modelName);
        element.put("type", modelTypeName);
        element.put("size", value);
        elements.add(element);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> getTypeStats() {
        return (Map<String, Object>) ctx().args.get(TOP_PER_TYPE_STATS);
    }

    @SuppressWarnings("unchecked")
    public static void storeStats() {
        long stop = System.currentTimeMillis();
        Map<String, Object> typeStats = getTypeStats();
        long start = (long) typeStats.get("start");
        typeStats.put("total", stop - start);

        Map<String, Object> map = (Map<String, Object>) typeStats.get("children");
        ArrayList<Object> children = new ArrayList<Object>();
        Set<String> keys = map.keySet();
        for (String key : keys) {
            HashMap<String, Object> child = new HashMap<String, Object>();
            Map<String, Object> typeMap = (Map<String, Object>) map.get(key);
            child.put("name", typeMap.get("name"));
            child.put("children", typeMap.get("children"));
            children.add(child);
        }
        typeStats.put("children", children);
        perTypeStats = typeStats;

        Map<String, Object> treeStats = (Map<String, Object>) ctx().args.get(TOP_CALL_TREE_STATS);
        treeStats.put("total", stop - start);
        treeStats.put("path", typeStats.get("path"));
        callTreeStats = treeStats;
    }

    public static void initStats(String path) {
        HashMap<String, Object> typeStats = new HashMap<String, Object>();
        typeStats.put("path", path);
        typeStats.put("name", path);
        typeStats.put("children", new HashMap<String, Object>());
        typeStats.put("start", System.currentTimeMillis());
        ctx().args.put(TOP_PER_TYPE_STATS, typeStats);
    }

    public static class Stats {
        public long timestamp;
        public long duration = 0;

        public Stats(long timestamp) {
            this.timestamp = timestamp;
        }
    }
}
