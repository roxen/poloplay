package controllers;

import java.util.List;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.page;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.policy.Policy;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

@ControllerFor({ "p.siteengine.Site", "p.siteengine.Page" })
public class PageController extends ModelController {
    public Html render(Model model) throws CMException {

        List<ContentId> contentIdList = Dispatcher.getContentPath();

        Model pageModel = null;
        Model topPageModel = null;
        Model siteModel = null;
        Model articleModel = null;
        Model rightColumn = null;
        for (ContentId contentId : contentIdList) {
            Model m = getPolicyModelDomain().getModel(contentId);
            String mt = m.getModelType().getName();
            if ("p.siteengine.Site".equals(mt)) {
                siteModel = m;
                pageModel = m;

                rightColumn = getSlotModelWithFallback(m, "pageLayout/selected/right", rightColumn);
            } else if ("p.siteengine.Page".equals(mt)) {
                pageModel = m;
                if (siteModel != null && topPageModel == null) {
                    topPageModel = m;
                }
                rightColumn = getSlotModelWithFallback(m, "pageLayout/selected/right", rightColumn);
            } else {
                articleModel = m;
            }
        }

        return page.render(pageModel, articleModel, siteModel, topPageModel, rightColumn);
    }

    @SuppressWarnings("unchecked")
    private Model getSlotModelWithFallback(Model m, String path, Model fallback) {
        Object candidate = ModelPathUtil.get(m, path);
        if (candidate != null && ((List<ContentId>) ModelPathUtil.get(m, path + "/slotElements/list")).size() > 0) {
            return (Model) candidate;
        }

        return fallback;
    }

    public static boolean contentPathContains(Object content) {
        if (content instanceof Model) {
            ContentId contentId = ((Policy) ModelPathUtil.get((Model) content, "_data")).getContentId().getContentId();
            return Dispatcher.getContentPath().contains(contentId);
        }

        return false;
    }
}
