package controllers;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.reflections.Reflections;

import play.Play;
import play.mvc.Result;
import play.twirl.api.Html;
import poloplay.BaseController;
import poloplay.ControllerFor;
import poloplay.ModelController;
import stats.StatsCollector;
import application.Global;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.polopoly.cm.ContentId;
import com.polopoly.cm.ExternalContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.collections.ContentList;
import com.polopoly.cm.path.PathTranslationException;
import com.polopoly.cm.policy.Policy;
import com.polopoly.model.Model;
import com.polopoly.siteengine.dispatcher.PathUtil;
import com.polopoly.siteengine.structure.SiteRoot;

public class Dispatcher extends BaseController {

    private static Logger LOG = Logger.getLogger(Dispatcher.class.getName());

    private final static NotImplementedController NOT_IMPLEMENTED_CONTROLLER = new NotImplementedController();
    private static final Map<String, ModelController> controllers = new HashMap<String, ModelController>();

    static {
        String packages = Play.application().configuration().getString("controllerPackages");
        if (packages != null) {
            Reflections reflections = new Reflections(packages);
            Set<Class<?>> controllerClasses = reflections.getTypesAnnotatedWith(ControllerFor.class);
            Injector injector = Guice.createInjector();
            for (Class<?> controllerClass : controllerClasses) {
                Object controller = injector.getInstance(controllerClass);
                if (controller instanceof ModelController) {
                    String[] modelTypeNames = controllerClass.getAnnotation(ControllerFor.class).value();
                    for (String modelTypeName : modelTypeNames) {
                        ModelController existingController = controllers.get(modelTypeName);
                        if (existingController == null) {
                            controllers.put(modelTypeName, (ModelController) controller);
                        } else {
                            LOG.warning(modelTypeName + " already mapped to " + existingController.getClass().getName());
                        }
                    }
                } else {
                    LOG.warning(controller + " not instance of " + ModelController.class.getName());
                }
            }
        } else {
            LOG.warning("No controller packages configured");
        }
    }

    private static final String CONTENT_PATH = "contentPath";

    private static ModelController getRenderer(String modelTypeName) {
        if (controllers.containsKey(modelTypeName)) {
            return controllers.get(modelTypeName);
        }
        return NOT_IMPLEMENTED_CONTROLLER;
    }

    @SuppressWarnings("unchecked")
    public static Result get(String path) throws CMException {
        try {
            List<ContentId> contentIdList;
            if (path == null) {
                contentIdList = getFirstSiteId();
                if (contentIdList.size() == 0) {
                    return notFound("Sites list is empty");
                }
            } else {
                contentIdList = getPathTranslator().getContentIdList(path);
            }
            ctx().args.put(CONTENT_PATH, contentIdList);

            if (shouldLogStats()) {
                StatsCollector.initStats(path);
            }

            Model model = getPolicyModelDomain().getModel(contentIdList.get(0));
            Result result = ok(render(model));

            if (shouldLogStats()) {
                StatsCollector.storeStats();
            }
            return result;
        } catch (PathTranslationException e) {
            return notFound(path);
        }
    }

    private static boolean shouldLogStats() {
        return request().queryString().get("stats") != null;
    }

    private static List<ContentId> getFirstSiteId() throws CMException {
        Policy sites = getCmServer().getPolicy(new ExternalContentId(Global.SITES_ID));
        ContentList sitesList = sites.getContent().getContentList();
        if (sitesList.size() > 0) {
            return Arrays.asList(sitesList.getEntry(0).getReferredContentId());
        } else {
            return Collections.emptyList();
        }
    }

    public static Html render(Model model) throws CMException {
        if (model instanceof Model) {
            Model realModel = (Model) model;
            String modelTypeName = realModel.getModelType().getName();
            ModelController renderer = getRenderer(modelTypeName);
            Html html;

            if (shouldLogStats()) {
                html = StatsCollector.render(renderer, realModel);
            } else {
                html = renderer.render(realModel);
            }

            return html;
        }

        LOG.log(Level.WARNING, "Could not render. " + model + " not instanceof " + Model.class.getName());
        return new Html("");
    }

    public static String path(Object model) {
        if (model instanceof Model) {
            Policy policy = (Policy) ((Model) model).getAttribute("_data");
            if (policy != null) {
                try {
                    String path = null;
                    ContentId contentId = policy.getContentId().getContentId();
                    try {
                        ContentId[] contentIds = PathUtil.createPathToClass(contentId, SiteRoot.class, Global.getCmServer());
                        path = getPathTranslator().createPath(contentIds);
                    } catch (IllegalStateException e) {
                        path = getPathTranslator().createPath(contentId);
                    }
                    String url = routes.Dispatcher.get(path.substring(1)).url();
                    if (shouldLogStats()) {
                        url += "?stats";
                    }
                    return url;
                } catch (CMException e) {
                    LOG.log(Level.WARNING, "Error creating path", e);
                }
            }
        } else {
            LOG.log(Level.WARNING, "Cannot create path. Not a model: " + model);
        }

        return "";
    }

    @SuppressWarnings("unchecked")
    public static List<ContentId> getContentPath() {
        return (List<ContentId>) ctx().args.get(CONTENT_PATH);
    }
}
