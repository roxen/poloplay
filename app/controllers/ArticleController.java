package controllers;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;

import views.html.article;

@ControllerFor("standard.Article")
public class ArticleController extends ModelController {

    @Override
    public Html render(Model model) throws CMException {
        return article.render(model);
    }
}
