package controllers;

import java.util.List;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.grid;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

@ControllerFor("com.atex.plugins.grid.MainElement")
public class GridController extends ModelController {
    public Html render(Model model) throws CMException {
        Integer columns = Integer.valueOf((String) ModelPathUtil.get(model, "numberOfColumns/value"));
        Integer rows = Integer.valueOf((String) ModelPathUtil.get(model, "numberOfRows/value"));
        @SuppressWarnings("unchecked")
        List<Model> list = (List<Model>) ModelPathUtil.get(model, "publishingQueue/list[0]/content/default/list");
        return grid.render(model, list, columns, rows, 12 / columns);
    }
}
