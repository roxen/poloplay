package controllers;

import java.util.List;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.list;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

@ControllerFor("com.atex.plugins.list.MainElement")
public class ListController extends ModelController {
    public Html render(Model model) throws CMException {
        @SuppressWarnings("unchecked")
        List<Object> contentList = (List<Object>) ModelPathUtil.get(model, "publishingQueue/list[0]/content/default/list");
        return list.render(model, contentList);
    }
}
