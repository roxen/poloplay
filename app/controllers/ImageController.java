package controllers;

import java.util.List;

import play.Play;
import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.image;

import com.atex.onecms.content.Content;
import com.atex.onecms.content.ContentManager;
import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.ContentVersionId;
import com.atex.onecms.content.IdUtil;
import com.atex.onecms.content.Subject;
import com.atex.onecms.image.AspectRatio;
import com.atex.onecms.image.ImageInfoAspectBean;
import com.atex.onecms.ws.image.ImageServiceConfigurationProvider;
import com.atex.onecms.ws.image.ImageServiceUrlBuilder;
import com.atex.standard.image.ImageContentDataBean;
import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

@ControllerFor("com.atex.onecms.content.AspectedPolicy")
public class ImageController extends ModelController {

    @Override
    public Html render(Model model) throws CMException {
        return render(model, null, null);
    }

    public static Html render(Model model, Integer width, Integer height) throws CMException {
        ContentId contentId = (ContentId) ModelPathUtil.get(model, "contentId");

        ContentManager contentManager = getCmClient().getContentManager();
        com.atex.onecms.content.ContentId oContentId = IdUtil.fromPolicyContentId(contentId);
        ContentVersionId versionId = contentManager.resolve(oContentId, Subject.NOBODY_CALLER);
        ContentResult<ImageContentDataBean> result = contentManager.get(versionId, null, ImageContentDataBean.class, null, Subject.NOBODY_CALLER);
        Content<ImageContentDataBean> content = result.getContent();
        ImageInfoAspectBean imageInfo = (ImageInfoAspectBean) content.getAspectData(ImageInfoAspectBean.ASPECT_NAME);

        if (imageInfo != null && imageInfo.getFilePath() != null) {
            ImageContentDataBean contentData = content.getContentData();

            try {
                String secret = new ImageServiceConfigurationProvider(getCmClient().getPolicyCMServer()).getImageServiceConfiguration().getSecret();
                ImageServiceUrlBuilder urlBuilder = new ImageServiceUrlBuilder(content, secret);
                
                AspectRatio defaultRatio = new AspectRatio(3, 2);
                Float paddingBottom = 100f;
                if (width != null && height != null) {
                    urlBuilder.aspectRatio(width, height);
                    paddingBottom = (float) height / (float) width * 100;
                } else {
                    urlBuilder.aspectRatio(3, 2);
                    paddingBottom = (float) defaultRatio.getHeight() / (float) defaultRatio.getWidth() * 100;
                }

                List<Integer> widths = Play.application().configuration().getIntList("image.sizes");
                String baseUrl = Play.application().configuration().getString("image.serviceBaseUrl");

                return image.render(widths, urlBuilder, contentData.getTitle(), paddingBottom, baseUrl);
            } catch (CMException e) {
                throw new RuntimeException("Secret not found", e);
            }
        }
        return new Html("Unable to render image");
    }
}
