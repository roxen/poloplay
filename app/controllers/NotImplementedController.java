package controllers;

import play.twirl.api.Html;
import poloplay.ModelController;
import views.html.notimplemented;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

public class NotImplementedController extends ModelController {
    public Html render(Model model) throws CMException {
        String type = model.getModelType().getName();
        Object object = ModelPathUtil.get(model, "name");
        String name = "[Unknown name]";
        if (object instanceof String) {
            name = String.valueOf(object);
        } else if (object instanceof Model) {
            name = ModelPathUtil.get((Model) object, "value").toString();
        }
        String id = "[Unknown content id]";
        ContentId contentId = (ContentId) ModelPathUtil.get(model, "contentId");
        if (contentId != null) {
            id = contentId.getContentIdString();
        }

        return notimplemented.render(type, name, id);
    }
}
