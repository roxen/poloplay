package controllers;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.contentlist;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;

@ControllerFor("p.ContentListEntryContainer")
public class ContentListController extends ModelController {
    public Html render(Model model) throws CMException {
        return contentlist.render(model);
    }
}
