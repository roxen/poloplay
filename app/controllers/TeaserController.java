package controllers;

import java.awt.Rectangle;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.teaser;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.VersionedContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;

@ControllerFor("com.atex.plugins.teaser.Teaser")
public class TeaserController extends ModelController {
    public Html render(Model model) throws CMException {
        Rectangle editorArea = null;
        Model imageModel = null;
        ContentId imageContentId = (ContentId) ModelPathUtil.get(model, "images/list[0]/content/contentId");
        if (imageContentId != null) {
            VersionedContentId versionedImageContentId = getCmServer().translateSymbolicContentId(imageContentId);
            imageModel = getPolicyModelDomain().getModel(versionedImageContentId);
        }

        Model articleModel = (Model) ModelPathUtil.get(model, "teaserables/list[0]/content");

        Object name = getWithFallback("name", model, articleModel);
        Object teaserText = ModelPathUtil.get(model, "teaser/value");
        if (teaserText == null && articleModel != null) {
            teaserText = ModelPathUtil.get(articleModel, "lead");
        }

        return teaser.render(model, articleModel, (String) name, (String) teaserText, imageModel, editorArea);
    }

    private static Object getWithFallback(String path, Model... models) {
        for (Model model : models) {
            Object hit = ModelPathUtil.get(model, path);
            if (hit != null) {
                return hit;
            }
        }

        return null;
    }
}
