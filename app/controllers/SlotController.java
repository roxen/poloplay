package controllers;

import play.twirl.api.Html;
import poloplay.ControllerFor;
import poloplay.ModelController;
import views.html.slot;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;

@ControllerFor("p.siteengine.layout.Slot.it")
public class SlotController extends ModelController {
    public Html render(Model model) throws CMException {
        return slot.render(model);
    }
}
