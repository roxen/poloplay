package application;

import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import play.Application;
import play.GlobalSettings;
import play.Play;
import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;

import com.polopoly.application.ConnectionProperties;
import com.polopoly.application.PacemakerComponent;
import com.polopoly.application.PacemakerSettings;
import com.polopoly.application.StandardApplication;
import com.polopoly.cm.client.CmClient;
import com.polopoly.cm.client.EjbCmClient;
import com.polopoly.cm.client.HttpCmClientHelper;
import com.polopoly.cm.client.HttpFileServiceClient;
import com.polopoly.cm.path.AbstractContentPathTranslator;
import com.polopoly.cm.path.ContentPathTranslator;
import com.polopoly.cm.path.impl.SiteEngineContentPathTranslator;
import com.polopoly.cm.policy.PolicyCMServer;
import com.polopoly.cm.policymvc.PolicyModelDomain;
import com.polopoly.pear.config.impl.ConfigReadBase;
import com.polopoly.search.solr.SolrIndexName;
import com.polopoly.search.solr.SolrSearchClient;

public class Global extends GlobalSettings {
    public static final String SITES_ID = "p.siteengine.Sites.d";

    private static final Logger LOG = Logger.getLogger(Global.class.getName());

    @Override
    public void onStart(Application application) {
        super.onStart(application);
        initApplication();
        initPathTranslator();
    }

    @Override
    public void onStop(Application application) {
        super.onStop(application);

        polopolyApplication.destroy();
        polopolyApplication = null;
        cmServer = null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[] { GzipFilter.class };
    }

    private static CmClient cmClient;
    private static PolicyCMServer cmServer;
    private static com.polopoly.application.Application polopolyApplication;
    private static ContentPathTranslator pathTranslator;
    private static PolicyModelDomain policyModelDomain;


    private static void initApplication() {
        try {

            polopolyApplication = new StandardApplication("poloplay");

            String connectionPropertiesUrl = Play.application().configuration().getString("p.connectionPropertiesUrl");
            ConnectionProperties connectionProperties = new ConnectionProperties(new URL(connectionPropertiesUrl));

            HttpCmClientHelper.createAndAddToApplication(polopolyApplication, connectionProperties);

            polopolyApplication.readConnectionProperties(connectionProperties);

            PacemakerComponent pacemaker = new PacemakerComponent();
            PacemakerSettings pacemakerSettings = new PacemakerSettings();
            pacemakerSettings.setEnabled(true);
            pacemaker.setPacemakerSettings(pacemakerSettings);
            polopolyApplication.addApplicationComponent(pacemaker);

            cmClient = (CmClient) polopolyApplication.getApplicationComponent(EjbCmClient.DEFAULT_COMPOUND_NAME);

            SolrSearchClient solrSearchClient = new SolrSearchClient(SolrSearchClient.DEFAULT_MODULE_NAME,
                    SolrSearchClient.DEFAULT_COMPONENT_NAME, getCmClient());
            solrSearchClient.setIndexName(new SolrIndexName("public"));
            polopolyApplication.addApplicationComponent(solrSearchClient);

            HttpFileServiceClient fileServiceClient = new HttpFileServiceClient();
            polopolyApplication.addApplicationComponent(fileServiceClient);

            polopolyApplication.init();

            cmServer = getCmClient().getPolicyCMServer();

            policyModelDomain = getCmClient().getPolicyModelDomain();

        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error initializing application", e);
        }
    }

    public static PolicyCMServer getCmServer() {
        return cmServer;
    }

    public static PolicyModelDomain getPolicyModelDomain() {
        return policyModelDomain;
    }

    private void initPathTranslator() {
        AbstractContentPathTranslator contentPathTranslator = new SiteEngineContentPathTranslator();
        contentPathTranslator.setPolicyCMServer(getCmServer());

        final HashMap<String, String> configMap = new HashMap<String, String>();
        configMap.put("rootContentId", SITES_ID);
        configMap.put("contentListNames", "polopoly.Department,default,pages,articles,feeds,landingPages");
        configMap.put("contentNameFallback", "true");
        configMap.put("ignoreCaseInPathSegment", "true");
        ConfigReadBase config = new ConfigReadBase() {

            @Override
            public String getString(String name) {
                if (configMap.containsKey(name)) {
                    return configMap.get(name);
                }

                return null;
            }

            @Override
            public String getString(String name, boolean mandatory) {
                return getString(name);
            }
        };
        contentPathTranslator.init(config);

        pathTranslator = contentPathTranslator;
    }

    public static ContentPathTranslator getPathTranslator() {
        return pathTranslator;
    }

    public static CmClient getCmClient() {
        return cmClient;
    }
}
