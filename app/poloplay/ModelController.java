package poloplay;

import play.twirl.api.Html;

import com.polopoly.cm.client.CMException;
import com.polopoly.model.Model;

public abstract class ModelController extends BaseController {
    public abstract Html render(Model model) throws CMException;
}
