package poloplay;

import play.mvc.Controller;
import application.Global;

import com.polopoly.cm.client.CmClient;
import com.polopoly.cm.path.ContentPathTranslator;
import com.polopoly.cm.policy.PolicyCMServer;
import com.polopoly.cm.policymvc.PolicyModelDomain;

public class BaseController extends Controller {
    protected static CmClient getCmClient() {
        return Global.getCmClient();
    }

    protected static PolicyCMServer getCmServer() {
        return Global.getCmServer();
    }

    protected static PolicyModelDomain getPolicyModelDomain() {
        return Global.getPolicyModelDomain();
    }

    protected static ContentPathTranslator getPathTranslator() {
        return Global.getPathTranslator();
    }
}
